import os
from os import path
from flask import Flask, render_template, jsonify, request
from pdf2image import convert_from_path
import main as m
from my_data import create_pth_file
import time
import shutil
app = Flask(__name__, template_folder='templates')

upload_folder = 'data_image'
extensions = {'pdf', 'png', 'jpg', 'jpeg'}

app.config['UPLOAD_FOLDER'] = upload_folder


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in extensions

@app.route('/preview', methods=['POST'])
def preview_image():
    req = request.get_json()
    if 'file' in req.keys():
        if allowed_file(req['file']) and path.exists('./static/images/' + req['file']):
            path_prefix = os.path.join("data_area" , str(int(round(time.time()*1000))))
            image_folder = os.path.join(path_prefix, app.config['UPLOAD_FOLDER'])
            check_path_create_if_not_exist(image_folder)
            shutil.copy('./static/images/' + req['file'], path_prefix + '/data_image')
            print("Image copied")
            returnDict = process_images(path_prefix)
            return jsonify(returnDict)
        else:
            return jsonify({"error": "Image Not Found"})
    else:
        return jsonify({"error": "No Image Provided"})

@app.route('/extract', methods=['POST'])
def extract_info():
    if 'file' not in request.files:
        return jsonify({'status': False})
    path_prefix = os.path.join("data_area" , str(int(round(time.time()*1000))))
    files = request.files.getlist('file')
    for file in files:
        if file.filename == '':
            return jsonify({'status': False})
    saved_files = []
    total_files = 0
    image_folder = os.path.join(path_prefix, app.config['UPLOAD_FOLDER'])
    print(image_folder)
    check_path_create_if_not_exist(image_folder)
    for file in files:
        total_files += 1
        if file and allowed_file(file.filename):
            # Check if pdf then convert to images else store it directly
            if '.' in file.filename and file.filename.rsplit('.', 1)[1].lower()=='pdf':
                file.save(os.path.join(image_folder, file.filename))
                images = convert_from_path(os.path.join(image_folder, file.filename))
                os.remove(os.path.join(image_folder, file.filename))
                i = 1
                for image in images:
                    image.save(os.path.join(image_folder, file.filename.rsplit('.', 1)[0]+'-image-' + str(i) + '.jpg'), 'JPEG')
                    saved_files.append(file.filename.rsplit('.', 1)[0]+'-image-pdf-' + str(i) + '.jpg')
                    i = i+1
                pass
            else:
                file.save(os.path.join(image_folder, file.filename))
                saved_files.append(file.filename)
    print("Image saved")
    returnDict = process_images(path_prefix)
    returnDict['total'] = total_files
    # for filename in saved_files:
    #     os.remove(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    #     os.remove(os.path.join("test_result", filename.split(".")[0]+".txt"))
    #     os.remove(os.path.join("task2_result", filename.split(".")[0]+".txt"))
    return jsonify(returnDict)

def check_path_create_if_not_exist(*paths):
    for path in paths:
        os.makedirs(path, exist_ok=True)

def process_images(prefix):
    print("processing images")
    image_path = os.path.join(prefix , "data_image")
    data_path_1 = os.path.join(prefix , "part1_result")
    data_path_2 = os.path.join(prefix , "part2_result")
    merged_data_path = os.path.join(prefix , "merged_data_and_boxing")
    pth_path = os.path.join(prefix , "pth/")
    model_pth_path = "model.pth"
    pth_file_path = os.path.join(pth_path , "data.pth")
    bounding_box_path = "boundingbox"
    result_path = os.path.join(prefix , "results")
    check_path_create_if_not_exist(image_path, data_path_1, data_path_2, merged_data_path, pth_path, result_path)

    m.load_images_to_predict(image_path, data_path_1)
    m.process_txt(data_path_1, data_path_2)
    m.merge_data_and_box(data_path_1, bounding_box_path, merged_data_path)
    create_pth_file(image_path, merged_data_path, pth_file_path)
    return m.prepare_json(model_pth_path,pth_file_path,result_path)

@app.route("/")
def index():
    return render_template('index.html')


if __name__ == "__main__":
    # app.run(debug=True)
    # app.run(host='127.0.0.1',port=8080, debug=True)
    app.run(host='0.0.0.0',port=80, debug=True)