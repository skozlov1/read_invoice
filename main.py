import torch
from torch.autograd import Variable
import utils
import dataset
import argparse
from my_data import MyDataset, VOCAB
from my_models import MyModel0
from my_utils import pred_to_dict
from PIL import Image
import json
import glob
import os
import csv
import cv2
import models.crnn as crnn


def predict_this_box(image, model, alphabet):
    converter = utils.strLabelConverter(alphabet)
    transformer = dataset.resizeNormalize((200, 32))
    image = transformer(image)
    if torch.cuda.is_available():
        image = image.cuda()
    image = image.view(1, *image.size())
    image = Variable(image)

    model.eval()
    preds = model(image)

    _, preds = preds.max(2)
    preds = preds.transpose(1, 0).contiguous().view(-1)

    preds_size = Variable(torch.IntTensor([preds.size(0)]))
    raw_pred = converter.decode(preds.data, preds_size.data, raw=True)
    sim_pred = converter.decode(preds.data, preds_size.data, raw=False)
    print('%-30s => %-30s' % (raw_pred, sim_pred))
    return sim_pred

def flatten2d(arr):
    for a in arr:
        for element in a:
            yield element

def load_images_to_predict(inputFolder, outputFolder):
    # load model
    model_path = './expr/netCRNN_199_423.pth'
    alphabet = '0123456789,.:(%$!^&-/);<~|`>?+=_[]{}"\'@#*ABCDEFGHIJKLMNOPQRSTUVWXYZ\ '
    imgH = 32 # should be 32
    nclass = len(alphabet) + 1
    nhiddenstate = 256

    model = crnn.CRNN(imgH, 1, nclass, nhiddenstate)
    if torch.cuda.is_available():
        model = model.cuda()
    print('loading pretrained model from %s' % model_path)
    model.load_state_dict({k.replace('module.',''):v for k,v in torch.load(model_path).items()})

    # load image
    filenames = [os.path.splitext(f)[0] for f in glob.glob(inputFolder+"/*.jpg")]
    jpg_files = [s + ".jpg" for s in filenames]
    print(jpg_files)
    for jpg in jpg_files:
        image = Image.open(jpg).convert('L')
        words_list = []
        with open('boundingbox/'+jpg.split(os.sep)[-1].split('.')[0]+'.txt', 'r') as boxes:
            for line in csv.reader(boxes):
                box = [int(string, 10) for string in line[0:8]]
                boxImg = image.crop((box[0], box[1], box[4], box[5]))
                words = predict_this_box(boxImg, model, alphabet)
                words_list.append(words)
        with open(outputFolder+os.sep+jpg.split(os.sep)[-1].split('.')[0]+'.txt', 'w+') as resultfile:
            for line in words_list:
                resultfile.writelines(line+'\n')
        


def process_txt(inputFolder, outputFolder):
    filenames = [os.path.splitext(f)[0] for f in glob.glob(inputFolder+"/*.txt")]
    old_files = [s + ".txt" for s in filenames]
    files = []
    data = []
    for old_file in old_files:
        new = []
        with open(old_file, "r") as old:
            for line in csv.reader(old):
                if not line:
                    continue
                if not line[0]:
                    continue
                if line[0][0] == ' ' or line[0][-1] == ' ':
                    line[0] = line[0].strip()
                if ' ' in line[0]:
                    line = line[0].split(' ')
                new.append(line)
        with open(outputFolder+os.sep + old_file.split(os.sep)[-1], "w+") as newfile:
            wr = csv.writer(newfile, delimiter = ',')
            new = [[s[0].upper()] for s in new]
            wr.writerow(new)
    #     files.append(old_file.split(os.sep)[1].split('.')[0]+".jpg")
    #     data.append(list(flatten2d(new)))
    # return {'files':files, 'data':data}


def merge_data_and_box(data_folder, boxing_folder, output_folder):
    filenames = [os.path.splitext(f)[0] for f in glob.glob(data_folder+"/*.txt")]
    box_files = [s + ".txt" for s in filenames]
    for boxfile in box_files:
        box = []
        with open(boxing_folder + os.sep + boxfile.split(os.sep)[-1],'r') as boxes:
            for line in csv.reader(boxes):
                box.append([int(string, 10) for string in line[0:8]])
        words = []
        with open(boxfile, 'r') as prediction:
            for line in csv.reader(prediction):
                words.append(line)
        words = [s if len(s)!=0 else [' '] for s in words]
        new = []
        for line in zip(box,words):
            a,b = line
            new.append(a+b)
        with open(output_folder + os.sep + boxfile.split(os.sep)[-1], 'w+') as newfile:
            csv_out = csv.writer(newfile)
            for line in new:
                csv_out.writerow(line)

def prepare_json(model_pth_file, data_pth_file, result_path):
    files = []
    data = []
    
    device = torch.device('cpu')

    model = MyModel0(len(VOCAB), 16, 256).to(device)
    dataset = MyDataset(None, device, data_pth_path=data_pth_file)

    model.load_state_dict(torch.load(model_pth_file))

    model.eval()
    with torch.no_grad():
        for key in dataset.data_dict.keys():
            text_tensor = dataset.get_data_from_file(key)
            oupt = model(text_tensor)
            prob = torch.nn.functional.softmax(oupt, dim=2)
            prob, pred = torch.max(prob, dim=2)

            prob = prob.squeeze().cpu().numpy()
            pred = pred.squeeze().cpu().numpy()

            real_text = dataset.data_dict[key]
            result = pred_to_dict(real_text, pred, prob)

            with open(result_path + os.sep + key + ".json", "w", encoding="utf-8") as json_opened:
                json.dump(result, json_opened, indent=4)
            files.append(key + ".jpg")
            data.append(result)
            print(key)
    return {'files':files, 'data':data}



if __name__ == "__main__":
    load_images_to_predict("data_test", "test_result")
    process_txt("test_result", "task2_result")
    print("Working")
    pass
